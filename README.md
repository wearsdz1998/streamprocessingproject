<<<<<<< README.md
﻿


<h3 align="center">Data engineering student project</h3>

<p align="center">

Application based on RDF graph with fake data, that are distributed using Apache Kafka and displayed with Grafana



<!-- ABOUT THE PROJECT -->

##  About The Project

Student project based on LUBM RDF graph with extended data using symbolic AI and onthology. This data is randomly generated and represents informations about vaccinated persons with side effects.

Theses informations constitute records which are transformated and distributing into streams using Apache Kafka.

Finally theses records can be stored in database in order to be displayed with a vizualisation tool.

<!-- GETTING STARTED -->

##  Getting Started

This section will show you how to execute the program.

###  Prerequisites

Before getting started, you need to download a copy of the latest version of the repository :

```sh

git clone https://gitlab.com/wearsdz1998/streamprocessingproject.git

```

In the following, it is assumed that you already have a running JDK on you machine.

You need to download a recent version of [Apache Zookeeper](https://www.apache.org/dyn/closer.cgi/zookeeper/) and [Apache Kafka](https://kafka.apache.org/downloads).

###  Installation

####  Zookeeper

In the Zookeeper /bin directory, launch the server with the following command :

```sh

./zkServer.sh start

```

You should get the following line:

```

Starting zookeeper ... STARTED

```

####  Kafka

Once Zookeeper is working properly, go to the Kafka installation /bin directory and type :

```sh

./kafka-server-start.sh -daemon ../config/server.properties

```

This will start the Kafka server. You don't need to worry about topics creation since the program already does.

<!-- USAGE EXAMPLES -->

##  Usage

Using SBT, compile the project with

```sh

sbt compile

```

It will download all dependencies of this application.

Then run it with

```sh

sbt run

```

Or you can use an IDE to build and run like IntelliJ.

___



You can now start different consummers according to the topics, or check the records in your IDE

<!-- SIDERS COUNT -->

##  SIDERS COUNT

In order to display all the siders effect numbers you need to start the following command

```sh

./kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic sidersCount --property print.key=true --property print.value=true --property value.deserializer=org.apache.kafka.common.serialization.LongDeserializer


```

Known issue : If you don't get any records when listening on the siders-count topic, comment the lines corresponding to the others streams

For instance, on the **Main.scala** file:

```scala
streams.open_stream("Vaccine-stream")  
streams.anonymous_stream("Vaccine-stream")  
streams.sider_stream("Vaccine-stream")  
streams.siders_count("Vaccine-stream-output")
```

And edit it:

```scala
streams.open_stream("Vaccine-stream")  
//streams.anonymous_stream("Vaccine-stream")  
//streams.sider_stream("Vaccine-stream")  
streams.siders_count("Vaccine-stream-output")
```


<!-- LICENSE -->

##  License

Distributed under the MIT License. See `LICENSE` for more information.

<!-- AUTHORS -->

##  Authors

Rameau Yohan

Gandega Singalle

Harouche Djahida

Ouksili Smail

Bouazza Mehdi