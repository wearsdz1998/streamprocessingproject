name := "Project"

version := "0.1"

scalaVersion := "2.13.8"

libraryDependencies += "org.apache.jena" % "jena-core" % "4.3.2"

libraryDependencies += "com.github.javafaker" % "javafaker" % "1.0.2"

libraryDependencies += "org.apache.kafka" %% "kafka" % "3.1.0"

libraryDependencies += "org.apache.kafka" % "kafka-streams" % "3.1.0"

libraryDependencies += "com.fasterxml.jackson.core" % "jackson-databind" % "2.12.0"

libraryDependencies += "com.fasterxml.jackson.core" % "jackson-core" % "2.13.1"
// https://mvnrepository.com/artifact/org.slf4j/slf4j-simple
libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.36"
