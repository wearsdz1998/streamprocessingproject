package fr.uge.stream.Kafka

import java.util.Properties
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}

object Producer {

    val props = new Properties()
    props.put("bootstrap.servers", "localhost:9092")
    props.put(ProducerConfig.CLIENT_ID_CONFIG, "Kafka Producer")
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
    val producer = new KafkaProducer[String, String](props)

    def send(topic : String, value : String) = {
      producer.send(new ProducerRecord(topic,value))
    }

    def closeProducer(): Unit ={
        producer.close()
    }

}