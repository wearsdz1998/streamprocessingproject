package fr.uge.stream.Kafka.Stream

import fr.uge.stream.Model.{Person}
import fr.uge.stream.Utils.SerdesPerson
import org.apache.kafka.common.serialization.{Serdes}
import org.apache.kafka.streams.kstream.{Consumed, KStream, Produced}
import org.apache.kafka.streams.{KafkaStreams, StreamsBuilder, StreamsConfig}

import java.time.Duration
import java.util.Properties

class StreamDSL {


    val serdes = new SerdesPerson
    val props = new Properties()
    props.put(StreamsConfig.APPLICATION_ID_CONFIG, "stream-pipe")
    props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
    props.putIfAbsent(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String.getClass.getName)
    props.putIfAbsent(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, serdes.getClass.getName)

    val builder = new StreamsBuilder()

  def open_stream(topic: String): Unit = {

    val kStream: KStream[String, Person] = builder.stream[String, Person](topic, Consumed.`with`(Serdes.String(), serdes))

      kStream.peek((k,v) => println(v)).to("Vaccine-stream-output", Produced.`with`(Serdes.String(), serdes))
      //Send unfiltered data to the topic

      val streams = new KafkaStreams(builder.build(), props)

      streams.start()
      streams.close(Duration.ofMinutes(1))
  }

  def anonymous_stream(topic: String): Unit = {
    val kStream: KStream[String, Person] = builder.stream[String, Person](topic, Consumed.`with`(Serdes.String(), serdes))

    kStream.peek((k,v) => println(v)).mapValues(value => Person(value.getId(), "anonymous", "anonymous", value.getGender(), value.getZipcode(), value.getState(), value.getBirthday(), value.getVaccinationDate(), value.getVaccine(), value.getSideEffectCode())).to("anonymousSideEffect", Produced.`with`(Serdes.String(), serdes))
    //Send filtered data to another topic

    val streams = new KafkaStreams(builder.build(), props)

    streams.start()
    streams.close(Duration.ofMinutes(1))

  }

  def sider_stream(topic: String): Unit = {
    val kStream: KStream[String, Person] = builder.stream[String, Person](topic, Consumed.`with`(Serdes.String(), serdes))

    kStream.peek((k,v) => println(v)).filter((k,v) => v.getSideEffectCode() == "C0027497").to("nauseaSideEffect", Produced.`with`(Serdes.String(), serdes))
    //Send filtered data to another topic

    val streams = new KafkaStreams(builder.build(), props)

    streams.start()
    streams.close(Duration.ofMinutes(1))
  }

  def siders_count(topic: String): Unit = {
    val textLines: KStream[String, Person] = builder.stream[String, Person](topic, Consumed.`with`(Serdes.String(), serdes))

    textLines.peek((k,v) => println(v)).groupBy((_, word) => word.getSideEffectCode()).count().toStream.to("sidersCount", Produced.`with`(Serdes.String(), Serdes.Long()))

    val streams: KafkaStreams = new KafkaStreams(builder.build(), props)
    streams.start()
    streams.close(Duration.ofMinutes(1))
  }

}
