package fr.uge.stream.Kafka

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper, SerializationFeature}
import fr.uge.stream.Model.Person
import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecords, KafkaConsumer}

import java.util
import java.util.Properties
import java.time.Duration

object Consumer {

    val props = new Properties()
    props.put("bootstrap.servers", "localhost:9092")
    props.put(ConsumerConfig.CLIENT_ID_CONFIG, "Kafka Consumer")
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
    props.put(ConsumerConfig.GROUP_ID_CONFIG, "1")
    props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest")

    val consumer = new KafkaConsumer[String, String](props)
    val jsonMapper = new ObjectMapper().disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
  jsonMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)

    def subscribe(topic : String): Unit = {
      consumer.subscribe(util.Arrays.asList(topic))
    }

    def read(): Unit = {
      while(true){
        val records: ConsumerRecords[String, String] = consumer.poll(Duration.ofMillis(1))
        records.forEach(x => {
          val p = jsonMapper.readValue(x.value(), Person.getClass)
        })
      }
    }

    def closeConsumer() = {
      consumer.close()
    }

}
