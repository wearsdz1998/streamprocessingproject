package fr.uge.stream

import fr.uge.stream.Faker.FakeDataWithOntology
import fr.uge.stream.Kafka.Stream.{StreamDSL}
import fr.uge.stream.Kafka.{Producer}
import fr.uge.stream.Utils.JacksonGenerator


object Main {
  var rdf_fake_data = new FakeDataWithOntology
  var streams = new StreamDSL

  def main (args: Array[String] ): Unit = {
    streams.open_stream("Vaccine-stream")
    streams.anonymous_stream("Vaccine-stream")
    streams.sider_stream("Vaccine-stream")
    streams.siders_count("Vaccine-stream-output")


    //rdf_fake_data.generate(45, 20, 50)

    val ressource = getClass().getClassLoader().getResource("lubm++.ttl").toString
    val path = ressource.substring(6)
    rdf_fake_data.read_from_file(path).foreach(p => {
      Producer.send("Vaccine-stream", JacksonGenerator.generate(p))
    })

  }

}
