package fr.uge.stream.Faker

import com.github.javafaker.Faker
import org.apache.jena.rdf.model.ModelFactory

import java.util.regex.Pattern

object FakeDataMain extends App {

  val file = getClass().getClassLoader().getResource("lubm1.ttl").toString
  val fileURI = file.substring(6)

  // create empty models
  val model = ModelFactory.createDefaultModel
  val profs = ModelFactory.createDefaultModel

  // use the RDFDataMgr to find the input file
  val in = model.read("file:///" + fileURI, "TTL")
  if (in == null) throw new IllegalArgumentException("File: " + fileURI + " not found")

  // read the RDF/XML file
  var iter = model.listStatements()
  val faker = new Faker();

  val propertyURI1 = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#identifier"
  val propertyURI2 = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#last_name"
  val propertyURI3 = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#first_name"
  val propertyURI4 = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#gender"
  val propertyURI5 = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#zipcode"
  val propertyURI6 = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#date_of_birth"

  val p1 = model.createProperty(propertyURI1)
  val p2 = model.createProperty(propertyURI2)
  val p3 = model.createProperty(propertyURI3)
  val p4 = model.createProperty(propertyURI4)
  val p5 = model.createProperty(propertyURI5)
  val p6 = model.createProperty(propertyURI6)

  //Size of the model before adding new properties
  System.out.println(model.size())

  while ( {
    iter.hasNext
  }) {
    val stmt = iter.nextStatement() // get next statement
    val subject = stmt.getSubject // get the subject

    if (Pattern.matches(".*UndergraduateStudent\\d", subject.getURI)) {

      val identifier = faker.number().randomDigit().toString
      val l_name = faker.name().lastName()
      val f_name = faker.name().firstName()
      val gender = faker.demographic().sex()
      val zipcode = faker.address().zipCode()
      val dob = faker.date().birthday(30, 70).toString

      val subjectURI = subject.getURI

      val s1 = model.createResource(subjectURI)

      val o1 = model.createResource(identifier)
      val o2 = model.createResource(l_name)
      val o3 = model.createResource(f_name)
      val o4 = model.createResource(gender)
      val o5 = model.createResource(zipcode)
      val o6 = model.createResource(dob)

      profs.add(model.createStatement(s1, p1, o1))
      profs.add(model.createStatement(s1, p2, o2))
      profs.add(model.createStatement(s1, p3, o3))
      profs.add(model.createStatement(s1, p4, o4))
      profs.add(model.createStatement(s1, p5, o5))
      profs.add(model.createStatement(s1, p6, o6))
    }

  }
  iter = profs.listStatements()
  while ( {
    iter.hasNext
  }) {
    val stmt = iter.nextStatement() // get next statement
    val subject = stmt.getSubject // get the subject
    val predicate = stmt.getPredicate // get the predicate
    val `object` = stmt.getObject // get the object

    //Display the new statements
    System.out.print(subject.toString)
    System.out.print(" " + predicate.toString + " ")
    System.out.print(" \"" + `object`.toString + "\"")
    System.out.println(" .")
  }

  //Adding the new model with extras properties to the old model
  model.add(profs)


  //Size of the model before after new properties
  System.out.println(model.size())
}
