package fr.uge.stream.Faker

import com.github.javafaker.Faker
import fr.uge.stream.Model.Person
import org.apache.jena.ontology.OntModelSpec
import org.apache.jena.rdf.model.ModelFactory

import java.io.{File, FileOutputStream}
import java.nio.file.Paths
import scala.collection.mutable.ListBuffer

class FakeDataWithOntology {

  val path = Paths.get("lubm++.ttl")
  val str = path.toAbsolutePath

  val bench = getClass().getClassLoader().getResource("univ-bench.owl").toString
  val benchURI = bench.substring(5)

  val lubm = getClass().getClassLoader().getResource("lubm1.ttl").toString
  val lubm1URI = bench.substring(5)

  // create empty models
  val person = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM_MICRO_RULE_INF)
  val model = ModelFactory.createDefaultModel

  // use the RDFDataMgr to find the input file
  val in = person.read("file:///" + benchURI)
  val lbm = model.read(lubm, "TTL")
  if (in == null || lbm == null) throw new IllegalArgumentException("File not found")

  // read the RDF/XML file
  val faker = new Faker()
  val kb = person.getOntClass("http://swat.cse.lehigh.edu/onto/univ-bench.owl#Person");
  var iter = kb.listSubClasses()

  val fakeData: FakeData = FakeData()

  val typ = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"

  val id = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#id"
  val firstname = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#firstname"
  val lastname = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#lastname"
  val gender = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#gender"
  val zipcode = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#zipcode"
  val state = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#state"
  val birthday = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#birthday"
  val vaccinationdate = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#vaccinationdate"
  val vaccine = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#vaccine"
  val sideeffect = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#sideeffect"

  def save_file(to_save: String) : Unit = {
    val file = new File(to_save)
    val os = new FileOutputStream(file)
    lbm.write(os, "N-TRIPLE")
  }

  def generate(maleProportion: Int, minAge: Int, maxAge: Int) : Unit = {
    while ( {
      iter.hasNext
    }) {
      val stmt = iter.next() // get next statement
      val instance = lbm.listResourcesWithProperty(lbm.getProperty(typ), lbm.getProperty(stmt.toString))
      instance.forEach(x => {
        if (!x.hasProperty(lbm.getProperty(vaccine))){
          lbm.add(lbm.createStatement(x, lbm.getProperty(id), lbm.createLiteral(fakeData.id().toString)))
          lbm.add(lbm.createStatement(x, lbm.getProperty(firstname), lbm.createLiteral(fakeData.firstname())))
          lbm.add(lbm.createStatement(x, lbm.getProperty(lastname), lbm.createLiteral(fakeData.lastname())))
          lbm.add(lbm.createStatement(x, lbm.getProperty(gender), lbm.createLiteral(fakeData.gender(maleProportion))))
          lbm.add(lbm.createStatement(x, lbm.getProperty(zipcode), lbm.createLiteral(fakeData.zipcode())))
          lbm.add(lbm.createStatement(x, lbm.getProperty(state), lbm.createLiteral(fakeData.state())))
          lbm.add(lbm.createStatement(x, lbm.getProperty(birthday), lbm.createLiteral(fakeData.birthday(minAge, maxAge))))
          lbm.add(lbm.createStatement(x, lbm.getProperty(vaccine), lbm.createLiteral(fakeData.vaccine())))
          lbm.add(lbm.createStatement(x, lbm.getProperty(vaccinationdate), lbm.createLiteral(fakeData.vaccinationDate())))
          lbm.add(lbm.createStatement(x, lbm.getProperty(sideeffect), lbm.createLiteral(fakeData.sideEffectCode())))
        }
      })
    }
    save_file(str.toString)
  }

  def read_from_file(to_read: String): ListBuffer[Person] = {
    val test = ModelFactory.createDefaultModel
    val model = test.read("file:///" + to_read, "N-TRIPLE")
    val lstResult: ListBuffer[Person] = new ListBuffer[Person]()

    model.listResourcesWithProperty(lbm.getProperty(vaccine)).forEach(p => {
      p.getProperty(lbm.getProperty(vaccine)).getLiteral
      p.getProperty(lbm.getProperty(id)).getLiteral

      val id_ = p.getProperty(lbm.getProperty(id)).getLiteral
      val firstname_ = p.getProperty(lbm.getProperty(firstname)).getLiteral
      val lastname_ = p.getProperty(lbm.getProperty(lastname)).getLiteral
      val gender_ = p.getProperty(lbm.getProperty(gender)).getLiteral
      val state_ = p.getProperty(lbm.getProperty(state)).getLiteral
      val zipcode_ = p.getProperty(lbm.getProperty(zipcode)).getLiteral
      val birthday_ = p.getProperty(lbm.getProperty(birthday)).getLiteral
      val vaccinDate_ = p.getProperty(lbm.getProperty(vaccinationdate)).getLiteral
      val vaccine_ = p.getProperty(lbm.getProperty(vaccine)).getLiteral
      val sideeffect_ = p.getProperty(lbm.getProperty(sideeffect)).getLiteral
      val person: Person = new Person(id_.toString, firstname_.toString, lastname_.toString, gender_.toString, zipcode_.toString, state_.toString, birthday_.toString, vaccinDate_.toString, vaccine_.toString, sideeffect_.toString)
      lstResult += person
    })

    lstResult
  }

}
