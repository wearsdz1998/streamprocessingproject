package fr.uge.stream.Model

import com.fasterxml.jackson.annotation.{JsonAlias, JsonProperty}
import fr.uge.stream.Faker.FakeData

case class Person(@JsonProperty("id") id: String,
             @JsonProperty("firstname")
             @JsonAlias(Array("firstName")) firstname: String,
             @JsonProperty("lastname")
             @JsonAlias(Array("lastName")) lastname: String,
             @JsonProperty("gender") gender: String,
             @JsonProperty("zipcode") zipcode: String,
             @JsonProperty("state") state: String,
             @JsonProperty("birthday") birthday: String,
             @JsonProperty("vaccinationDate") vaccinationDate: String,
             @JsonProperty("vaccine") vaccine: String,
             @JsonProperty("sideEffectCode") sideEffectCode: String = null) {


  val vaccinated: Boolean = !vaccinationDate.isBlank

  def generateRecordWithFakeSideEffect: Person = {
    Person(id, firstname, lastname, gender, zipcode, state, birthday, vaccinationDate, vaccine, sideEffectCode)
  }

  def getId() : String = {
    id
  }

  def getFirstName() : String = {
    firstname
  }

  def getLastName() : String = {
    lastname
  }

  def getGender() : String = {
    gender
  }

  def getZipcode() : String = {
    zipcode
  }

  def getState() : String = {
    state
  }

  def getBirthday() : String = {
    birthday
  }

  def getVaccinationDate() : String = {
    vaccinationDate
  }

  def getVaccine() : String = {
    vaccine
  }

  def getSideEffectCode() : String = {
    sideEffectCode
  }

}

  object Person {
    val fakeData: FakeData = FakeData()
    val maleProportion = 40
    val vaccinesProportion = 70

    // According to lubm1 data, the following types correspond to students
    val studentTypes = List("GraduateStudent", "UndergraduateStudent", "TeachingAssistant", "ResearchAssistant")
    // According to lubm1 data, the following types correspond to professors
    val professorTypes = List("AssociateProfessor", "FullProfessor", "Lecturer", "AssistantProfessor")

    private def oneContains(lstA: List[String], lstB: List[String]) : Boolean = {
      for (a <- lstA) if (lstB.contains(a)) return true
      false
    }


    def isStudent(personTypes: List[String]) : Boolean = oneContains(personTypes, studentTypes)
    def isProfessor(personTypes: List[String]) : Boolean = oneContains(personTypes, professorTypes)

    def apply(personTypes: List[String]) : Person = {
      val minAge = if (isStudent(personTypes)) 20 else if (isStudent(personTypes)) 25 else if (isProfessor(personTypes)) 30 else throw new IllegalArgumentException("Invalid person type")
      val maxAge = if (isStudent(personTypes) || isStudent(personTypes)) 30 else if (isProfessor(personTypes)) 70 else throw new IllegalArgumentException("Invalid person type")
      val isVaccinated = fakeData.faker.random.nextInt(101) < vaccinesProportion

      new Person(fakeData.id().toString,
        fakeData.firstname(),
        fakeData.lastname(),
        fakeData.gender(maleProportion),
        fakeData.zipcode(),
        fakeData.state(),
        fakeData.birthday(minAge, maxAge),
        if (isVaccinated) fakeData.vaccinationDate() else "",
        if (isVaccinated) fakeData.vaccine() else "",
        if (isVaccinated) fakeData.sideEffectCode() else "No side effect")
    }
}