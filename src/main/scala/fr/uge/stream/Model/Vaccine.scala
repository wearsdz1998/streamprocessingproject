package fr.uge.stream

import scala.language.implicitConversions

package object Vaccine extends Enumeration {
  type Vaccine = Value

  val PFIZER = Value("Pfizer")
  val MODERNA = Value("Moderna")
  val ASTRAZENECA = Value("AstraZeneca")
  val SPOUTNIKV = Value("SpoutnikV")
}