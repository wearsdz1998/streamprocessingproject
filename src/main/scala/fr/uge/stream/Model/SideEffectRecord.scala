package fr.uge.stream.Model

case class SideEffectRecord(id: String,
                            firstname: String,
                            lastname: String,
                            gender: String,
                            zipcode: String,
                            state: String,
                            birthday: String,
                            vaccinationDate: String,
                            vaccine: String,
                            sideEffectCode: String = null) {
}
