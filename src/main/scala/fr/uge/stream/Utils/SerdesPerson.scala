package fr.uge.stream.Utils

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import fr.uge.stream.Model.Person
import org.apache.kafka.common.serialization.{Deserializer, Serde, Serializer}

class SerdesPerson extends Serde[Person]{
  val mapper = new ObjectMapper().registerModule(DefaultScalaModule)

  override def serializer(): Serializer[Person] = {
    (_: String, data: Person) => mapper.writeValueAsBytes(data)
  }

  override def deserializer(): Deserializer[Person] = {
    (_: String, data: Array[Byte]) => mapper.readValue(data, classOf[Person])
  }
}
