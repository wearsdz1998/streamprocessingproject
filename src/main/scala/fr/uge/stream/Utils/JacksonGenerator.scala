package fr.uge.stream.Utils

import com.fasterxml.jackson.core.JsonProcessingException
import fr.uge.stream.Model.Person
import com.fasterxml.jackson.databind.{ObjectMapper, SerializationFeature}

object JacksonGenerator {

  def generate(person:Person) : String = {
    val jsonMapper = new ObjectMapper().disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
    try {
      return jsonMapper.writeValueAsString(person);
    }
    catch {
      case x: JsonProcessingException => {
        x.getMessage
      }
    }
  }

}
